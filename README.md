# Rock-Paper-Scissors

[Live Demo](http://www.joevargas.io/RockPaperScissors/index.html)

Rock-Paper-Scissors game created using HTML, CSS and JS...but mostly JS :)

This project taught me how to use HTML DOM manipulation and functions that interact with other functions. My favorite part was using `switch` over `if` and `else` statements. For example, `switch` was used in the case where it compared the user's selected and computer's random(`Math.floor(Math.random()*3)`) choice of rock, paper and scissors. Using `case` results, it would trigger off either a `win()`, `lose()`, or `draw()` function. Within those functions, the results would display with HTML DOM manipulation using classes along side css styling. 

![](/demo/rps_demo.gif)